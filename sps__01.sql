USE [Navistar_Finanzas]
GO
/****** Object:  StoredProcedure [dbo].[sps_GenerarHistorico]    Script Date: 8/17/2021 10:59:49 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




ALTER PROCEDURE [dbo].[sps_GenerarHistorico] 
@cd_proceso as bigint,
@nu_compania as int
AS

--exec [sps_GenerarHistorico] @cd_proceso = 911953, @nu_compania = 929

-- delete from THFIN_Polizas where id_registroBitacora = 911953
-- sps_GenerarTraduccionPolizas '929'
--select * from THFIN_Polizas where id_registroBitacora = 911953

SET NOCOUNT ON

BEGIN 

		DECLARE @anio as int
		DECLARE @periodo as int
		DECLARE @txLbor as varchar(5)

		BEGIN TRY
			SELECT @anio = CAST(F1 as int), @periodo = CAST(f2 as int) FROM TWFIN_Polizas where id_registro = 1
		END try
		BEGIN CATCH
		 	SET @anio = 2000
		    SET @periodo = 1
		END CATCH


		 SET @txLbor = CAST(@nu_compania as varchar(5))
		
		--SE OBTIENE LA ESTRUCTURA DE LA TABLA
   		--DROP TABLE #Traduccion
		CREATE TABLE #Traduccion(
			[id_registro] [bigint]   NULL,
			[id_registroBitacora] [int] NULL,
			[F1] [nvarchar](400) NULL,
			[F2] [nvarchar](400) NULL,
			[F3] [nvarchar](400) NULL,
			[F4] [nvarchar](400) NULL,
			[F5] [nvarchar](400) NULL,
			[F6] [nvarchar](400) NULL,
			[F7] [nvarchar](400) NULL,
			[F8] [nvarchar](400) NULL,
			[F9] [nvarchar](400) NULL,
			[F10] [nvarchar](400) NULL,
			[F11] [nvarchar](400) NULL,
			[F12] [nvarchar](400) NULL,
			[F13] [nvarchar](400) NULL,
			[F14] [nvarchar](400) NULL,
			[F15] [nvarchar](400) NULL,
			[F16] [nvarchar](400) NULL,
			[F17] [nvarchar](400) NULL,
			[F18] [nvarchar](400) NULL,
			[F19] [nvarchar](400) NULL,
			[F20] [nvarchar](400) NULL,
			[F21] [nvarchar](400) NULL,
			[F22] [nvarchar](400) NULL,
			[F23] [nvarchar](400) NULL
		)
				
		INSERT  #Traduccion
		EXEC sps_GenerarTraduccionPolizas @Lbor = @txLbor




		INSERT INTO THFIN_Polizas
		(
			   [id_registroBitacora]
			  ,[nu_compania]
			  ,[F1]
			  ,[F2]
			  ,[F3]
			  ,[F4]
			  ,[F5]
			  ,[F6]
			  ,[F7]
			  ,[F8]
			  ,[F9]
			  ,[F10]
			  ,[F11]
			  ,[F12]
			  ,[F13]
			  ,[F14]
			  ,[F15]
			  ,[F16]
			  ,[F17]
			  ,[F18]
			  ,[F19]
			  ,[F20]
			  ,[F21]
			  ,[F22]
			  ,[F23]
			  ,[nu_anio]
			  ,[nu_periodo]
		)
  
        SELECT @cd_proceso
			  ,@nu_compania
			  ,[F1]
			  ,[F2]
			  ,[F3]
			  ,[F4]
			  ,[F5]
			  ,[F6]
			  ,[F7]
			  ,[F8]
			  ,[F9]
			  ,[F10]
			  ,[F11]
			  ,[F12]
			  ,[F13]
			  ,[F14]
			  ,[F15]
			  ,[F16]
			  ,[F17]
			  ,[F18]
			  ,[F19]
			  ,[F20]
			  ,[F21]
			  ,[F22]
			  ,'' as [F23] 
			  ,@anio
			  ,@periodo
			  FROM #Traduccion


		/*SELECT @cd_proceso
			  ,@nu_compania
			  ,[F1]
			  ,[F2]
			  ,[F3]
			  ,[F4]
			  ,[F5]
			  ,[F6]
			  ,[F7]
			  ,[F8]
			  ,[F9]
			  ,[F10]
			  ,[F11]
			  ,[F12]
			  ,[F13]
			  ,[F14]
			  ,[F15]
			  ,[F16]
			  ,[F17]
			  ,[F18]
			  ,[F19]
			  ,[F20]
			  ,[F21]
			  ,[F22]
			  ,[F23] 
			  ,@anio
			  ,@periodo
			  FROM [dbo].[TWFIN_Polizas]
			  */

	DROP TABLE #Traduccion

	exec spi_GeneraResumenPolizas
	@cdregistroBitacora = @cd_proceso
	,@nuLbor = @nu_compania
	
END




