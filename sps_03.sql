USE [Navistar_Finanzas]
GO
/****** Object:  StoredProcedure [dbo].[spi_GeneraResumenPolizas]    Script Date: 8/17/2021 11:03:24 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

/*
exec spi_GeneraResumenPolizas
@cdregistroBitacora = 123123
,@nuLbor = 998
*/

ALTER PROCEDURE [dbo].[spi_GeneraResumenPolizas] 
    @cdregistroBitacora as int,
	@nuLbor as int
as
BEGIN

/*

select * from  [Navistar_Finanzas].[dbo].[THFIN_Polizas]
where id_registroBitacora = 123123 and f2 like '%0215%'

select * from tefin_polizas where id_registroBitacora = 911829


SELECT  max(id_registroBitacora) FROM [Navistar_Finanzas].[dbo].[THFIN_Polizas]
   WHERE nu_anio = 2015 AND nu_periodo = 3
		 AND nu_compania = 963

select * from [THFIN_Polizas] where id_registroBitacora = 911829

delete from [THFIN_Polizas] where id_registroBitacora = 123123

*/


--CREACION DEL RESUMEN A PARTIR DEL ARCHIVO QUE COMPARTE EL CORPORATIVO
	INSERT INTO [Navistar_Finanzas].[dbo].[TEFIN_Polizas]
	(
		   [id_registroBitacora]
		  ,[nu_compania]
		  ,[nu_anio]
		  ,[nu_periodo]
		  ,[tx_poliza]
		  ,[tx_otyp]
		  ,[nu_odoc]
		  ,[fh_fecha]
		  ,[tx_conceptoPoliza]
		  ,[tx_numeroCuenta]
		  ,[tx_conceptoTransaccion]
		  ,[to_debe]
		  ,[to_haber]
		  ,[tx_BaaNTTYP]
		  ,[tx_PSTTYP]
		  ,[nu_ocurrencias]
	)

	SELECT 
		   [id_registroBitacora]
		  ,[nu_compania]
		  ,[nu_anio]
		  ,[nu_periodo]
		  ,[F1] as [tx_poliza]
		  ,null as [tx_otyp]
		  ,null as [nu_odoc]
		  ,REPLACE(REPLACE([F2],'0014-','2014-'),'0215','2015') as [fh_fecha]
		  ,[F3] as [tx_conceptoPoliza]
		  ,[F4] as [tx_numeroCuenta]
		  ,[F5] as [tx_conceptoTransaccion]
		  
		  --,SUM(CAST([F6] as decimal(28,10))) as [F6]
		  --,SUM(-1 * CAST([F7] as decimal(28,10))) as [F7]
		  ,(CAST([F6] as decimal(28,10))) as [F6]
		  ,(-1 * CAST([F7] as decimal(28,10))) as [F7]
		  
		  ,[F9] as [tx_BaaNTTYP]
		  ,[F10] as [tx_PSTTYP]
		  
		  --,count(*)  
		  ,1 as [nu_ocurrencias]
	  FROM [Navistar_Finanzas].[dbo].[THFIN_Polizas]
	  WHERE id_registroBitacora = @cdregistroBitacora AND nu_compania = @nuLbor
	  AND ISNUMERIC([F6]) != 0
	  --and isdate(f2) = 0
	  /*GROUP BY
	  [id_registroBitacora]   ,[nu_compania]
	   ,[nu_anio]    ,[nu_periodo]
	   ,[F1]    ,[F2]    ,[F3]
	   ,[F4]    ,[F5]    ,[F9]   ,[F10]*/


IF @nuLbor IN (963,908,904,513)
BEGIN


UPDATE TEFIN_Polizas
   SET [tx_otyp] = [dbo].[String.Split]([tx_poliza],'-',0) 
	  ,[nu_odoc] = [dbo].[String.Split]([tx_poliza],'-',1) 
  WHERE id_registroBitacora = @cdregistroBitacora
        AND nu_compania = @nuLbor


--DROP TABLE #Referencia
	SELECT 
	id_registro,to_debe,to_haber
	,0 as tp_cambioDebe
	,0 as tp_cambioHaber
	INTO #Referencia 
	FROM TEFIN_Polizas
	WHERE tx_numeroCuenta like 'SRNT%'
	AND id_registroBitacora = @cdregistroBitacora
        AND nu_compania = @nuLbor

/*
Don Rodolfo ya vi la lógica del sistema del corporativo, de favor hacer lo siguiente:
Únicamente aplicará para cuentas que comiencen con SRNT y que los importes en el debe o haber estén con signo negativo
Los valores en el debe con signo negativo pasarlos al haber como valores positivos
Los valores en el haber con sino negativo pasarlos al debe como valores positivos
*/

	UPDATE  #Referencia
		SET  tp_cambioDebe  = CASE WHEN to_debe < 0  THEN 1 ELSE 0 END
			,tp_cambioHaber = CASE WHEN to_haber < 0 THEN 1 ELSE 0 END
			,to_Haber  = CASE WHEN to_debe < 0  THEN to_debe * -1 ELSE to_Haber END
			,to_debe  = CASE WHEN to_haber < 0  THEN to_haber * -1 ELSE to_debe END
	
	UPDATE  #Referencia
		SET  to_debe  = CASE WHEN tp_cambioDebe = 1  THEN 0 ELSE to_debe END
			,to_haber  = CASE WHEN tp_cambioHaber = 1  THEN 0 ELSE to_Haber END

	UPDATE  a
	SET a.to_debe  = b.to_debe
	   ,a.to_haber = b.to_haber
	FROM TEFIN_Polizas as a JOIN #Referencia as b
	ON(a.id_registro = b.id_registro)

END
ELSE 
BEGIN 

--ACTUALIZACION DE LOS CAMPOS DE TRANSACCION Y DOCUMENTO EN BASE AL CAMPO DE POLIZA
   
UPDATE TEFIN_Polizas
   SET [tx_otyp] = [dbo].[String.Split]([tx_poliza],'-',0) 
	  ,[nu_odoc] = [dbo].[String.Split]([tx_poliza],'-',1) 
  WHERE id_registroBitacora = @cdregistroBitacora AND nu_compania = @nuLbor

END



END




