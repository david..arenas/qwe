USE [Navistar_Finanzas]
GO
/****** Object:  StoredProcedure [dbo].[sps_GenerarTraduccionPolizas]    Script Date: 8/17/2021 11:02:39 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

--exec sps_GenerarTraduccionPolizas '963'
ALTER PROCEDURE [dbo].[sps_GenerarTraduccionPolizas] 
@Lbor as varchar(5)
AS

--exec sps_GenerarTraduccion


SET NOCOUNT ON

BEGIN 
 
	DECLARE @nu_compania  as int 
	DECLARE @tipoCompania as int
	
	
	
	SELECT top 1 @nu_compania = cd_compania ,
	   @tipoCompania = CASE WHEN tx_sistemaOrigen = 'BAAN' THEN 1 ELSE 2 END
	   FROM TCFIN_Empresa WHERE tx_Lbor = @Lbor
	

	
	IF @tipoCompania = 1	
	BEGIN
		SELECT 
		       id_registro 
			  ,id_registroBitacora
			  ,[F1]    
			  ,[F2] 
			  --,CAST((SELECT  tx_desc FROM [Navistar_Finanzas].[dbo].[VCDMC_Transaccion] WHERE nu_compania = @nu_compania AND tx_ttyp = a.F9) as NVARCHAR(400)) as [F3] 
			  ,w.[tx_desc] as [F3]
			  ,[F4]  
			  ,CAST(CASE WHEN d.[tx_desc] IS NULL THEN a.[F5] 
			             ELSE d.[tx_desc]  END as nvarchar(400)) as [F5] 
			  ,[F6] 
			  ,[F7] 
			  ,[F8]
			  ,[F9]
			  ,[F10]
			  ,[F11]
			  ,[F12]
			  ,[F13]
			  ,[F14] 
			  ,[F15] 
			  ,[F16] 
			  ,[F17] 
			  ,[F18] 
			  ,[F19] 
			  ,[F20] 
			  ,[F21] 
			  ,[F22] 
			  ,[F23] 
		  FROM [Navistar_Finanzas].[dbo].[TWFIN_Polizas]
		
		   as a LEFT JOIN
		  (
		     SELECT b.* FROM 
			    [Navistar_Finanzas].[dbo].[VCDMC_Transaccion] as b
			    LEFT JOIN 
				[Navistar_Finanzas].[dbo].[TCFIN_ExcluyeTransaccion] as c
				ON(b.tx_ttyp = c.tx_ttyp AND  c.st_estado = 'ACT' AND b.nu_compania = @nu_compania AND c.nu_compania = @nu_compania)
				WHERE c.cd_Transaccion is null AND b.nu_compania = @nu_compania
		  ) as d
		  
		  ON(Rtrim(Ltrim(a.F9)) = d.tx_ttyp)
		  JOIN [Navistar_Finanzas].[dbo].[VCDMC_Transaccion] as w
		  ON(w.nu_compania = @nu_compania AND Rtrim(Ltrim(a.F9)) = w.tx_ttyp)
		  WHERE a.id_registro > 2
		  ORDER BY a.id_registro asc
		
		  
	END
	ELSE
	BEGIN
	    
		SELECT [id_registro]
			  ,[id_registroBitacora]
			  ,[F1]
			  ,[F2]
			  ,CAST(CASE WHEN d.[tx_Traduccion] IS NULL THEN a.[f3] 
			             ELSE d.[tx_Traduccion] END as nvarchar(400))  as [F3]
			  ,[F4]
			  ,CAST(CASE WHEN d.[tx_Traduccion] IS NULL THEN a.[f5] 
			             ELSE d.[tx_Traduccion] END as nvarchar(400))  as [F5]
			  ,[F6]
			  ,[F7]
			  ,[F8]
			  ,[F9]
			  ,[F10]
			  ,[F11]
			  ,[F12]
			  ,[F13]
			  ,[F14]
			  ,[F15]
			  ,[F16]
			  ,[F17]
			  ,[F18]
			  ,[F19]
			  ,[F20]
			  ,[F21]
			  ,[F22]
			  ,[F23]
			  
		  FROM [Navistar_Finanzas].[dbo].[TWFIN_Polizas] as a LEFT JOIN
		  (
		     SELECT b.* FROM 
			    [Navistar_Finanzas].[dbo].[TCFIN_RemplazoPeople] as b
			       WHERE st_estado = 'ACT' 
			    
		  ) as d
		  ON(Rtrim(Ltrim(a.F10)) = d.tx_JournalSourceID AND Rtrim(Ltrim(a.[F5])) = d.tx_Descripcion)
		  WHERE a.id_registro > 2 
		  order by a.id_registro asc
	
	END


END




